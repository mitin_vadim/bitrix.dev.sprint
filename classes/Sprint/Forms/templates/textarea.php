<tr>
    <td>
        <?=$title?>
        <?if ($is_required):?><small >*</small><?endif?>
    </td>
    <td>
        <?$APPLICATION->IncludeComponent("bitrix:fileman.light_editor","",array(
            "CONTENT" => $value,
            "INPUT_NAME" => $name,
            "INPUT_ID" => $id,
            "WIDTH" => "100%",
            "HEIGHT" => "200px",
            "RESIZABLE" => "Y",
            "AUTO_RESIZE" => "N",
            "VIDEO_ALLOW_VIDEO" => "N",
            "VIDEO_MAX_WIDTH" => "640",
            "VIDEO_MAX_HEIGHT" => "480",
            "VIDEO_BUFFER" => "20",
            "VIDEO_LOGO" => "",
            "VIDEO_WMODE" => "transparent",
            "VIDEO_WINDOWLESS" => "Y",
            "VIDEO_SKIN" => "/bitrix/components/bitrix/player/mediaplayer/skins/bitrix.swf",
            "USE_FILE_DIALOGS" => "N",
            "ID" => "",
            "JS_OBJ_NAME" => "",
            "TOOLBAR_CONFIG" => array(
                'Bold', 'Strike', 'RemoveFormat','CreateLink', 'DeleteLink',
            )

        ));?>
        <?if (!empty($errors)):?><div ><?=implode(', ', $errors)?></div><?endif?>
        <?if (!empty($note)):?><div ><?=$note?></div><?endif?>
    </td>
</tr>