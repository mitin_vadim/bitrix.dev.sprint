<tr>
    <td>
        <?=$title?>
        <?if ($is_required):?><small >*</small><?endif?>
    </td>
    <td>
        <select  name="<?=$name?>[start]">
            <?foreach ($items as $key=>$val):?>
                <option <?if ($key == $value['start']):?>selected="selected"<?endif?> value="<?=$key?>" ><?=$val?></option>
            <?endforeach;?>
        </select>
        &mdash;
        <select  name="<?=$name?>[end]">
            <?foreach ($items as $key=>$val):?>
                <option <?if ($key == $value['end']):?>selected="selected"<?endif?> value="<?=$key?>" ><?=$val?></option>
            <?endforeach;?>
        </select>
        <?if (!empty($errors)):?><div ><?=implode(', ', $errors)?></div><?endif?>
        <?if (!empty($note)):?><div ><?=$note?></div><?endif?>
    </td>
</tr>