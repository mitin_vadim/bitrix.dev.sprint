<tr>
    <td>
        <?=$title?>
        <?if ($is_required):?><small >*</small><?endif?>
    </td>
    <td>
        <input readonly="readonly"  name="<?=$name?>" id="<?=$id?>" type="text" value="<?=$value?>" />

        <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
                "SHOW_INPUT" => "N",
                "FORM_NAME" => $form_name,
                "INPUT_NAME" => $name,
                "INPUT_VALUE" => $value,
                "SHOW_TIME" => "Y",
                "HIDE_TIMEBAR" => "Y"
            )
        );?>
        <?if (!empty($errors)):?><div ><?=implode(', ', $errors)?></div><?endif?>
        <?if (!empty($note)):?><div ><?=$note?></div><?endif?>
    </td>
</tr>
