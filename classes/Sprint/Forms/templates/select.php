<tr>
    <td>
        <?=$title?>
        <?if ($is_required):?><small >*</small><?endif?>
    </td>
    <td>
        <select  name="<?=$name?>" id="<?=$id?>">
            <?if (!$is_required):?><option value="0" >--нет--</option><?endif?>
            <?foreach ($items as $key=>$val):?>
                <option <?if ($key == $value):?>selected="selected"<?endif?> value="<?=$key?>" ><?=$val?></option>
            <?endforeach;?>
        </select>

        <?if (!empty($errors)):?><div ><?=implode(', ', $errors)?></div><?endif?>
        <?if (!empty($note)):?><div ><?=$note?></div><?endif?>
    </td>
</tr>
