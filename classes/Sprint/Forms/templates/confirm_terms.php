<tr>
    <td>
        <?if ($is_required):?><small >*</small><?endif?>
    </td>
    <td>
        <input type="checkbox" <?if ($value == 'Y'):?>checked="checked"<?endif?> id="<?=$id?>" name="<?=$name?>" value="Y" >
        <label for="<?=$id?>" >
            <?=$message?>
        </label>

        <?if (!empty($errors)):?><div ><?=implode(', ', $errors)?></div><?endif?>
        <?if (!empty($note)):?><div ><?=$note?></div><?endif?>
    </td>
</tr>