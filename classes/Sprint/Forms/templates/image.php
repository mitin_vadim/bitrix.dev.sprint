<tr>
    <td>
        <?=$title?>
        <?if ($is_required):?><small >*</small><?endif?>
    </td>
    <td>
        
        <?if (!empty($value['id'])): $img = \CFile::ResizeImageGet($value['id'], array("width"=>$width, "height"=>$height))?>
            <img src="<?=$img['src'];?>">
            <input type="hidden" value="<?=$value['id']?>" name="<?=$name?>[id]">            
            <label>
                <input type="checkbox" name="<?=$name?>[del]" <?if ($value['del'] == 'Y'):?>checked="checked"<?endif?> value="Y" /> 
                Удалить картинку
            </label>            
        <?endif?>

        <?$APPLICATION->IncludeComponent(
	        "bitrix:main.file.input", 
	        "drag_n_drop", 
	        array(
		        "INPUT_NAME" => $name . '_new',
		        "MULTIPLE" => "N",
		        "MODULE_ID" => "iblock",
		        "MAX_FILE_SIZE" => "10000000",
		        "ALLOW_UPLOAD" => "I",
		        "ALLOW_UPLOAD_EXT" => ""
	        ),
	        false
        );?>        


        
        <?if (!empty($errors)):?><div ><?=implode(', ', $errors)?></div><?endif?>
        <?if (!empty($note)):?><div ><?=$note?></div><?endif?>
    </td>
</tr>
