<?php

namespace Sprint\Forms\Validators;
use Sprint\Forms\Validator;

class Number extends Validator {

    public function isValid($value){

        if ($this->isEmpty($value) || is_numeric($value)){
            return true;
        }

        return false;
    }
}

