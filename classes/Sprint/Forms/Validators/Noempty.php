<?php

namespace Sprint\Forms\Validators;
use Sprint\Forms\Validator;

class Noempty extends Validator {

    public function isValid($value){

        if (!$this->isEmpty($value) && 0 !== $value){
            return true;
        }

        return false;
    }
}

