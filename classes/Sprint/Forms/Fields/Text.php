<?php

namespace Sprint\Forms\Fields;

use Sprint\Forms\Field;

class Text extends Field{

    protected function initialize(){
        $this->setTemplate('text');
    }

	protected function bindValue($value){
		return $value;
	}
}

