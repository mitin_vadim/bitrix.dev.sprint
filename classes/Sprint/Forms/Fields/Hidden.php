<?php

namespace Sprint\Forms\Fields;

use Sprint\Forms\Field;

class Hidden extends Field{

    protected function initialize(){
        $this->setTemplate('hidden');
    }

	protected function bindValue($value){
		return $value;
	}

}

