<?php

namespace Sprint\Forms\Fields;

use Sprint\Forms\Field;

class Select extends Field{

    protected function initialize(){
        $this->setTemplate('select',array('items'));
    }

    public function bindValue($value){
		$items = $this->getParam('items', array());
		return (array_key_exists($value, $items)) ? $value : '';
	}
}