<?php

namespace Sprint\Forms\Fields;

use Sprint\Forms\Field;

class TextArea extends Field{

    protected function initialize(){
        $this->setTemplate('textarea');
    }

	protected function bindValue($value){
		return $value;
	}
}

