<?php

namespace Sprint\User;
use Sprint\Image;

class ElementsDbResult {

	/* @var \CDBResult */
	protected $dbResult = null;

    protected $bTextHtmlAuto = true;
	protected $personalPhotoSize = array('width' => 0, 'height' => 0, 'exact' => 0);

	public function __construct(\CDBResult $dbResult){
		$this->dbResult = $dbResult;
	}

    public function getCountItems(){
        return $this->dbResult->SelectedRowsCount();
    }

    public function getCountItemsOnPage(){
        return $this->dbResult->NavPageSize;
    }

    public function getNavCurrentPage(){
        return $this->dbResult->NavPageNomer;
    }

    public function getNavCountPages(){
        return $this->dbResult->NavPageCount;
    }

    public function getNavRecordCount(){
        return $this->dbResult->NavRecordCount;
    }

	public function setFetchParams($bTextHtmlAuto = true){
		$this->bTextHtmlAuto = $bTextHtmlAuto;
		return $this;
	}

	public function setPersonalPhotoSize($width, $height, $exact=0) {
		$this->personalPhotoSize = array('width' => intval($width), 'height' => intval($height), 'exact' => $exact);
		return $this;
	}

	public function fetch(){
		if ($aItem = $this->dbResult->GetNext($this->bTextHtmlAuto)){

			if(!empty($aItem['NAME'])){
				$aItem['NAME'] = htmlspecialchars_decode($aItem['NAME'], ENT_QUOTES);
			}
			
			if(!empty($aItem['PERSONAL_PHOTO'])){
				$aItem['PERSONAL_PHOTO'] = Image::resizeImageById($aItem['PERSONAL_PHOTO'], $this->personalPhotoSize['width'], $this->personalPhotoSize['height'], $this->personalPhotoSize['exact']);
			}

			$aItem = $this->prepareFetch($aItem);
		}
		return $aItem;
	}

	public function fetchAll($indexKey = false){
		$list = array();

		while ($aItem = $this->fetch()){
			if ($indexKey){
				$list[ $aItem[$indexKey] ] = $aItem;
			} else {
				$list[] = $aItem;
			}
		}

		return $list;
	}


    protected function prepareFetch($aItem){
        return $aItem;
    }
}