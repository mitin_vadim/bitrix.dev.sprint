<?php

namespace Sprint\Helpers;

class AdminFormHelper {

	private $tabIndex = -1;
	private $tabCode = '';
	private $sectionIndex = 0;
	private $fields = array();
	private $iblockId = 0;
	
	private $properties = array();
	
	protected $tabs = array(); 

	protected $titles = array(
		'ACTIVE' => 'Активность',
		'ACTIVE_FROM' => 'Начало активности',
		'ACTIVE_TO' => 'Окончание активности',
		'NAME' => 'Название',

		'PREVIEW_PICTURE' => 'Картинка для анонса',
		'PREVIEW_TEXT' => 'Описание для анонса',
		'DETAIL_PICTURE' => 'Детальная картинка',
		'DETAIL_TEXT' => 'Детальное описание',

		'SECTIONS' => 'Разделы',

		'SORT' => 'Сортировка',
		'CODE' => 'Символьный код',
		'TAGS' => 'Теги',
	);

	public function __construct($iblockId){
		$this->iblockId = $iblockId;

		$dbResult = \CIBlockProperty::GetList(array("sort"=>"asc"), array("IBLOCK_ID"=>$iblockId, "CHECK_PERMISSIONS" => "N"));
		while ($aItem = $dbResult->GetNext(true, false)){
			$this->titles['PROPERTY_' . $aItem['ID']] = $aItem['NAME'];
			$this->properties[$aItem['CODE']] = $aItem;
		}
	}
	
	public function addTab($title){
		$this->tabIndex++;
		$this->sectionIndex = 0;
		$this->tabCode = 'edit' . ($this->tabIndex + 1);

		$val = ($this->tabIndex == 0) ? $this->tabCode : '--' . $this->tabCode;
		$val .= '--#--' . $title . '--';
		$this->tabs[$this->tabIndex] = array(
			$val
		);
		return $this;
	}
	
	protected function addTabIfEmpty(){
		if (empty($this->tabs[$this->tabIndex])){
			$this->addTab('Элемент');
		}
	}

	public function addFields($fields, $titles = array()){
		if (is_array($fields)){
			foreach ($fields as $key => $val){
				$title = isset($titles[$key]) ? $titles[$key] : '';
				$this->addField($val, $title);
			}
		}
		return $this;
	}

	public function addField($code, $title = ''){
		if (false !== strpos($code, 'PROPERTY_')){

			$code = str_replace('PROPERTY_', '', $code);
			if (intval($code) <=0 ){
				$code = isset($this->properties[$code]) ? $this->properties[$code]['ID'] : 0;
			}

			$code = 'PROPERTY_' . $code;
		}

		if (empty($title)){
			$title = isset($this->titles[$code]) ? $this->titles[$code] : $code;
		}

		if (!in_array($code, $this->fields)){
			$this->addTabIfEmpty();
			$val = '--' . $code . '--#--' . $title . '--';
			$this->tabs[$this->tabIndex][] = $val;
		}

		$this->fields[] = $code;
		return $this;
	}

	public function mergeTitles($titles=array()){
		$this->titles = array_merge($this->titles, $titles);
		return $this;
	}

	public function addSection($title){
		$this->addTabIfEmpty();
		$val = '--' . $this->tabCode . 'csection'. ($this->sectionIndex + 1) . '--#----' . $title . '--';
		$this->tabs[$this->tabIndex][] = $val;
		$this->sectionIndex++;
		return $this;
	}

	public function addTabExtra($title){
		$this->addTab($title);
		foreach ($this->titles as $key=>$val){
			$this->addField($key, $val);
		}
		return $this;
	}

	public function getOptions(){
		$opts = array();
		foreach ($this->tabs as $aTab){
			$opts[] = implode(',', $aTab);
		}

		$opts = implode(';', $opts) . ';--';
		return $opts;
	}

	public function execute(){
		$category = 'form';
		$name = 'form_element_' . $this->iblockId;
		$value = array(
			'tabs' => $this->getOptions()
		);

        \CUserOptions::DeleteOptionsByName($category, $name);
        \CUserOptions::SetOption($category, $name, $value, true);

	}



}