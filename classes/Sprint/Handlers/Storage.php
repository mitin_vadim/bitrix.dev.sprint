<?php

namespace Sprint\Handlers;

class Storage {

	static public function onAfterModifyIblock(&$arFields){
		if (!empty($arFields['ID'])){
            \Sprint\Storage::getIblock()->clear();
		}
	}

}