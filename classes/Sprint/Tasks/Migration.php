<?php

namespace Sprint\Tasks;
use Sprint\Task;

use Sprint\Migration\Manager;

class Migration extends Task {

    private $manager = null;

    /* @return Manager*/
    protected function getMigrationManager(){
        if (is_null($this->manager)){
            $this->manager = new Manager();
        }
        return $this->manager;
    }

    public function executeList(){
        $versions = $this->getMigrationManager()->getVersions();
        foreach ($versions as $item){
            $color = str_replace(array('is_new', 'is_success', 'is_404'), array('red', 'green', 'light_purple'), $item['type']);
            $name = $item['version'];
            $this->out('[%s]%s[/]', $color, $name);
        }
    }

    public function executeStatus(){
        $this->executeList();
    }

    public function executeUp(){
        $ok = $this->getMigrationManager()->executeNextVersionUp();
        $this->out($ok ? '[green]success[/]' : '[red]error[/]');
    }

    public function executeDown(){
        $ok = $this->getMigrationManager()->executeNextVersionDown();
        $this->out($ok ? '[green]success[/]' : '[red]error[/]');
    }


    public function executeDownAll(){
        $this->executeMigrate('--down');
    }

    public function executeUpAll(){
        $this->executeMigrate('--up');
    }

    public function executeMigrate($up='--up', $stopVersion = ''){
        if ($up == '--up') {
            $cnt = 0;
            while ($this->getMigrationManager()->executeNextVersionUp($stopVersion)){
                $cnt++;
            }
            $this->out('Migrations up: [green]%d[/]', $cnt);

        } elseif ($up == '--down'){
            $cnt = 0;
            while ($this->getMigrationManager()->executeNextVersionDown($stopVersion)){
                $cnt++;
            }
            $this->out('Migrations down: [red]%d[/]', $cnt);

        } else {
            $this->out('[red]required params not found[/]');
        }
    }

    public function executeExecute($version, $up='--up'){
        if ($version && $up == '--up'){
            $ok = $this->getMigrationManager()->executeVersion($version, true);
            $this->out($ok ? 'success' : 'error');
        } elseif ($version && $up == '--down') {
            $ok = $this->getMigrationManager()->executeVersion($version, false);
            $this->out($ok ? 'success' : 'error');
        } else {
            $this->out('[red]required params not found[/]');
        }
    }

    public function executeGenerate($descr = ''){
        $ok = $this->getMigrationManager()->createVersionFile($descr);
        $this->out($ok ? 'success' : 'error');
    }

    public function executeAdd($descr = ''){
        $this->executeGenerate($descr);
    }
}



