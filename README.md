Модуль содержит механизм миграций, помогающий синхронизировать изменения между нескольким копиями бд. 
--------------------------------------------------------------------------------------------------------
вики проекта https://bitbucket.org/andrey_ryabin/bitrix.dev.sprint/wiki/Home

страничка в блогах http://dev.1c-bitrix.ru/community/webdev/user/39653/blog/11245/ 

работаем с миграциями 
https://bitbucket.org/andrey_ryabin/bitrix.dev.sprint/wiki/Migration 

создаем файл-консоль 
https://bitbucket.org/andrey_ryabin/bitrix.dev.sprint/wiki/Console


Установка 
-------------
через маркеплейс, модуль Спринт (dev.sprint)

или

git submodule add git@bitbucket.org:andrey_ryabin/bitrix.dev.sprint.git bitrix/modules/dev.sprint
админка - Marketplace - Установленные решения - Спринт - установить

