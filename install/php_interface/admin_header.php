<?php

if (\CModule::IncludeModule('dev.sprint')) {

    $adminHeader = new Sprint\AdminHeader($_SERVER['DOCUMENT_ROOT'] . '/include_areas/admin/');
    $adminHeader->execute(array(
        'file1' => 'bitrix/admin',
    ));
}