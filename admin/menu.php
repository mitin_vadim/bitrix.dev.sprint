<?
global $APPLICATION;

IncludeModuleLangFile(__FILE__);

if($APPLICATION->GetGroupRight("dev.sprint")!="D") {
	$aMenu = array(
		"parent_menu" => "global_menu_services",
		"section" => "Sprint",
		"sort" => 50,
		"text" => GetMessage('DEVSPRINT_MENU_SPRINT'),
        "icon" => "sys_menu_icon",
        "page_icon" => "sys_page_icon",
		"items_id" => "sprint_migration_manager",
		"items" => array(
			array(
				"text" => GetMessage('DEVSPRINT_MENU_MIGRATION_MANAGER'),
				"url" => "sprint_migration_manager.php?lang=".LANGUAGE_ID,
			),

		)
	);

	return $aMenu;
}

return false;
