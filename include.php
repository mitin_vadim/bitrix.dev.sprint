<?php

CModule::IncludeModule("iblock");

//autoload psr-0
require_once __DIR__ .  '/classes/Sprint/Loader.php';
$classLoader = new Sprint\Loader('Sprint', __DIR__ . '/classes');
$classLoader->register();

AddEventHandler("iblock", "OnAfterIBlockUpdate", Array("Sprint\\Handlers\\Storage", "onAfterModifyIblock"));
AddEventHandler("iblock", "OnAfterIBlockAdd", Array("Sprint\\Handlers\\Storage", "onAfterModifyIblock"));